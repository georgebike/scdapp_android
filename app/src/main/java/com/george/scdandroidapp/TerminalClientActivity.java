package com.george.scdandroidapp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyLog;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.GeoApiContext;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.cert.TrustAnchor;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class TerminalClientActivity extends AppCompatActivity implements OnMapReadyCallback{
    Utils utils;
    TerminalClientTask mTerminalClientTask;
    DeleteLocationTask mDeleteLocationTask;
    Location mLastLocation;
    AutomaticLocationUpdater autoLocationUpdater;
    int selectedMarkerId = -1;
    Marker selectedMarker = null;

    private GoogleMap mMap = null;
    private MapView mMapView = null;

    private EditText mMessageView;
    private Button mAutomaticLocationButton;
    private Button mManualLocationButton;
    private Button mDeleteAccountButton;
    private Button mStopAutomaticLocationButton;
    private Button mDeleteLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terminal_client);

        utils = new Utils(this);
        final LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // Ask for user permission to use location services
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        final LocationListener locationListener = new GpsLocationListener();

        mMessageView = (EditText) findViewById(R.id.txtMessage);
        mAutomaticLocationButton = (Button) findViewById(R.id.btnAutomaticLocation);
        mManualLocationButton = (Button) findViewById(R.id.btnManualLocation);
        mDeleteAccountButton = (Button) findViewById(R.id.btnDeleteAccount);
        mStopAutomaticLocationButton = (Button) findViewById(R.id.btnStopAutomaticLocation);
        mDeleteLocation = (Button) findViewById(R.id.btnDeleteLocation);

        Bundle mapViewBundle = null;
        if(savedInstanceState != null){
            mapViewBundle = savedInstanceState.getBundle(getString(R.string.google_maps_key));
        }
        mMapView = findViewById(R.id.mapView);
        mMapView.onCreate(mapViewBundle);
        mMapView.getMapAsync(this);

        mAutomaticLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(autoLocationUpdater != null) return;
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    System.out.println("PERMISSION PROBLEMS!!!");
                    Toast.makeText(getApplicationContext(), "Problem with the location permissions", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (locationManager != null) {
                    Toast.makeText(getApplicationContext(), "Started requestLocationUpdates", Toast.LENGTH_SHORT).show();
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 1, locationListener);
                    autoLocationUpdater = new AutomaticLocationUpdater(7000);
                    autoLocationUpdater.start();
                }
            }
        });

        mStopAutomaticLocationButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(autoLocationUpdater != null){
                    autoLocationUpdater.killLocationUpdater();
                    autoLocationUpdater = null;
                    Toast.makeText(getApplicationContext(), "Stopped automatic location updater", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mManualLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    System.out.println("PERMISSION PROBLEMS!!!");
                    Toast.makeText(getApplicationContext(), "Problem with the location permissions", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (locationManager != null) {
                    Toast.makeText(getApplicationContext(), "Started requestLocationUpdates", Toast.LENGTH_SHORT).show();
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 1, locationListener);
                }
                attemptPostLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            }
        });

        mDeleteLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedMarkerId > 0){
                    attemtpDeleteLocation(selectedMarkerId);
                }
                else {
                    Toast.makeText(getApplicationContext(), "Please select a location first!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void attemptPostLocation(double latitude, double longitude){
        String message;
        String jwtToken = getIntent().getStringExtra("jwt_token");

        if(mMessageView.getText() != null)
            message = mMessageView.getText().toString();
        else message = null;

        /* If for some reason there is already an open Async Task handling the REST requests abort */
        if(mTerminalClientTask != null){
            if(mTerminalClientTask.getStatus() == AsyncTask.Status.RUNNING) return;
        }else{  // if there isn't already an instance of TerminalClientTask create a new one
            mTerminalClientTask = new TerminalClientTask(message, latitude, longitude, jwtToken);
        }

        mTerminalClientTask.execute((Void) null);
    }

    private void attemtpDeleteLocation(int locationId){
        String jwtToken = getIntent().getStringExtra("jwt_token");

        if(mDeleteLocationTask != null){
            if(mDeleteLocationTask.getStatus() == AsyncTask.Status.RUNNING) return;
        } else {
            mDeleteLocationTask = new DeleteLocationTask(locationId, jwtToken);
        }
        mDeleteLocationTask.execute((Void) null);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                JSONObject currentMarkerObject;
                currentMarkerObject = (JSONObject)marker.getTag();
                try {
                    selectedMarkerId = (int) (currentMarkerObject != null ? currentMarkerObject.get("id") : null);
                    selectedMarker = marker;
                } catch (JSONException e) {
                    e.printStackTrace();
                    return false;
                }
                return false;
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(getString(R.string.google_maps_key));
        if(mapViewBundle == null){
            mapViewBundle = new Bundle();
            outState.putBundle(getString(R.string.google_maps_key), mapViewBundle);
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onStart(){
        super.onStart();
        mMapView.onStart();
    }

    @Override
    protected void onStop(){
        super.onStop();
        mMapView.onStop();
    }

    @Override
    protected void onPause(){
        super.onPause();
        mMapView.onPause();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory(){
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    /* Class to handle Device's GPS */
    private class GpsLocationListener implements LocationListener{

        GpsLocationListener(){
            mLastLocation = new Location(LocationManager.GPS_PROVIDER);
        }

        @Override
        public void onLocationChanged(Location location) {
            mLastLocation.set(location);
            System.out.println("[OnLocationChanged()] LAT: " + location.getLatitude() + "LONG: " + location.getLongitude());
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }

    /* Thread that will periodically post a new location */
    class AutomaticLocationUpdater extends Thread{
        int msPeriod;
        boolean isAlive;
        AutomaticLocationUpdater(int period) {
            this.msPeriod = period;
            this.isAlive = true;
        }

        public void run (){
            while(isAlive){
                /* Call the method that will attempt to post the location by using Asych Task */
                attemptPostLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                try {
                    Thread.sleep(msPeriod);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void killLocationUpdater(){
            this.isAlive = false;
        }
    }

    /* Async task that will handle the API requests without blocking the UI thread */
    private class TerminalClientTask extends AsyncTask<Void, Void, Boolean>{
        CountDownLatch cdLatch = new CountDownLatch(1);

        private final String message;
        private final double latitude;
        private final double longitude;
        private final String jwtToken;
        private boolean locationPublished;
        JSONObject jsonLocationPublishResp;

        TerminalClientTask(String message, double latitude, double longitude, String jwtToken){
            this.message = message;
            this.latitude = latitude;
            this.longitude = longitude;
            this.jwtToken = jwtToken;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            if (utils != null){
                utils.setCdLatch(cdLatch);
                utils.RequestPublishLocation(message, latitude, longitude, jwtToken, new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONObject jsonResult) {
                        locationPublished = true;
                        jsonLocationPublishResp = jsonResult;

                        try {
                            mMap.addMarker(new MarkerOptions().position(
                                    new LatLng((double)jsonLocationPublishResp.get("latitude"),
                                            (double)jsonLocationPublishResp.get("longitude"))).title(jsonLocationPublishResp.getString("info"))).setTag(jsonLocationPublishResp);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(JSONObject jsonResult) {
                        locationPublished = false;
                        jsonLocationPublishResp = jsonResult;
                    }

                    @Override
                    public void onDeleted(String deletedMessage) {
                        //
                    }
                });
                try{
                    cdLatch.await(4000, TimeUnit.MILLISECONDS);
                }catch (InterruptedException ex){
                    ex.printStackTrace();
                }
                return locationPublished;
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean locationPublishedSuccess){
            if(locationPublishedSuccess){
                Toast.makeText(getApplicationContext(), "Location Added", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(getApplicationContext(), "Failed to add location!", Toast.LENGTH_SHORT).show();
            }
            mTerminalClientTask = null;
        }

        @Override
        protected void onCancelled() {
            mTerminalClientTask = null;
        }
    }

    /* Async task that will handle the API requests without blocking the UI thread */
    private class DeleteLocationTask extends AsyncTask<Void, Void, Boolean>{
        CountDownLatch cdLatch = new CountDownLatch(1);

        private final String jwtToken;
        private int locationId;
        private boolean locationDeleted;
        String locationDeletedResponse;

        DeleteLocationTask(int locationId, String jwtToken){
            this.locationId = locationId;
            this.jwtToken = jwtToken;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            if (utils != null){
                utils.setCdLatch(cdLatch);
                utils.RequestDeleteLocation(locationId, jwtToken, new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONObject jsonResult) {
                        //
                    }

                    @Override
                    public void onFailure(JSONObject jsonResult) {
                        //
                    }

                    @Override
                    public void onDeleted(String deletedMessage) {
                        if(deletedMessage.toLowerCase().contains("not")){
                            locationDeleted = false;
                            locationDeletedResponse = deletedMessage;
                        }
                    }
                });
                try{
                    cdLatch.await(4000, TimeUnit.MILLISECONDS);
                }catch (InterruptedException ex){
                    ex.printStackTrace();
                }
                return locationDeleted;
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean locationPublishedSuccess){
            if(locationDeleted){
                Toast.makeText(getApplicationContext(), "Location deleted successfully", Toast.LENGTH_SHORT).show();
                selectedMarker.setVisible(false);
                selectedMarker.remove();
            }else {
                Toast.makeText(getApplicationContext(), "Failed to delete location!", Toast.LENGTH_SHORT).show();
            }
            mDeleteLocationTask = null;
        }

        @Override
        protected void onCancelled() {
            mDeleteLocationTask = null;
        }
    }
}

