package com.george.scdandroidapp;

import android.content.Context;
import java.lang.String;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.MapFragment;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class Utils {
    private Context context;
    CountDownLatch cdLatch = null;

    public Utils(Context context){
        this.context = context;
    }

    public void setCdLatch(CountDownLatch cdLatch){
        this.cdLatch = cdLatch;
    }

    /* POST Login method */
    public void RequestLogin(String username, String password, final VolleyCallback callback){
        JSONObject jsonCredentials = new JSONObject();
        try{
            jsonCredentials.put("username", username);
            jsonCredentials.put("password", password);
        }catch (JSONException ex){
            ex.printStackTrace();
        }
        JsonObjectRequest jsonLoginRequest = new JsonObjectRequest(context.getString(R.string.user_login_url), jsonCredentials, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response){
                try{
                    if (response.has("jwt_token")){
                        callback.onSuccess(response);
                    }
                    else{
                        callback.onFailure(response);
                    }
                    cdLatch.countDown();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this.context);
        requestQueue.add(jsonLoginRequest);
    }

    /* POST Register User Method */
    public void RequestRegister(String username, String password, boolean isTerminal, final VolleyCallback callback){
        JSONObject jsonCredentials = new JSONObject();

        try{
            jsonCredentials.put("username", username);
            jsonCredentials.put("password", password);
            jsonCredentials.put("is_terminal", isTerminal);
        }catch (JSONException ex){
            ex.printStackTrace();
        }
        JsonObjectRequest jsonRegisterRequest = new JsonObjectRequest(context.getString(R.string.users_url), jsonCredentials, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.has("jwt_token")) {
                        callback.onSuccess(response);
                    } else {
                        callback.onFailure(response);
                    }
                    cdLatch.countDown();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                System.out.println(error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this.context);
        requestQueue.add(jsonRegisterRequest);
    }

    /* POST Publish New Location */
    public void RequestPublishLocation(String message, double latitude, double longitude, final String jwtToken, final VolleyCallback callback){

        JSONObject jsonLocationInfo = new JSONObject();

        try{
            jsonLocationInfo.put("info", message);
            jsonLocationInfo.put("latitude", latitude);
            jsonLocationInfo.put("longitude", longitude);
        }catch (JSONException ex){
            ex.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(context.getString(R.string.locations_url), jsonLocationInfo, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response);
                try{
                    if(response.has("owner_id")){
                        callback.onSuccess(response);
                    } else {
                        callback.onFailure(response);
                    }
                    cdLatch.countDown();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                System.out.println(error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("api-token", jwtToken);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this.context);
        requestQueue.add(jsonObjectRequest);
    }

    /* DELETE Location (by ID) Method */
    public void RequestDeleteLocation(final int locationId, final String jwtToken, final VolleyCallback callback){
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, context.getString(R.string.locations_url) + String.valueOf(locationId),
                new Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("AM STERS LOCATIA CU ID-UL " + String.valueOf(locationId));
                        callback.onDeleted("Location Deleted!");
                        cdLatch.countDown();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                callback.onDeleted("Location NOT Deleted!");
            }
        })  {
               @Override
               public Map<String, String> getHeaders() throws AuthFailureError{
                   Map<String, String> params = new HashMap<String, String>();
                   params.put("Content-Type", "application/json");
                   params.put("api-token", jwtToken);
                   return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this.context);
        requestQueue.add(stringRequest);
    }
}
