package com.george.scdandroidapp;

import org.json.JSONObject;

public interface VolleyCallback{
    void onSuccess(JSONObject jsonResult);
    void onFailure(JSONObject jsonResult);
    void onDeleted(String deletedMessage);
}
