package com.george.scdandroidapp;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaRouter;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class LoginActivity extends AppCompatActivity {
    Utils utils;

    private UserLoginTask mAuthTask;
    private EditText mUsernameView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private Button mLoginButton;
    private Button mRegisterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Instantiate the Utils class
        utils = new Utils(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form
        mUsernameView = (EditText) findViewById(R.id.txtUsername);
        mPasswordView = (EditText) findViewById(R.id.txtPassword);
        mLoginButton = (Button) findViewById(R.id.btnLogin);
        mRegisterButton = (Button) findViewById(R.id.btnRegister);

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });

        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                LoginActivity.this.startActivity(registerIntent);
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    private void attemptLogin(){
        if (mAuthTask != null){
            return;
        }

        // Reset errors
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store the values at the time of the login attempt
        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for valid password
        if(!isUsernameValid(username)){
            mUsernameView.setError(getString(R.string.error_invalid_username)); // get username error string
            focusView = mUsernameView;
            cancel = true;
        }else if(!isPasswordValid(password)){
            mPasswordView.setError(getString(R.string.error_invalid_password)); // get password error string
            focusView = mPasswordView;
            cancel = true;
        }

        if(cancel){
            // An error occured. Don't attempt login and focus the first form field with an error
            focusView.requestFocus();
        }else{
            // Show Progress Spinner and kick off a background task to perform the user login attempt
            showProgress(true);
            mAuthTask = new UserLoginTask(username, password);
            mAuthTask.execute((Void) null);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private boolean isUsernameValid(String username){
        if(TextUtils.isEmpty(username)){
            return false;
        }
        return true;
    }

    private boolean isPasswordValid(String password){
        if(TextUtils.isEmpty(password)){
            return false;
        }else if(password.length()<5){
            return false;
        }
        return true;
    }

    private class UserLoginTask extends AsyncTask<Void, Void, Boolean>{
        private final String mUsername;
        private final String mPassword;
        private JSONObject jsonLoginResponse;
        private boolean loginValidated;

        CountDownLatch cdLatch = new CountDownLatch(1);

        UserLoginTask(String username, String password){
            this.mUsername = username;
            this.mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            if(utils != null){
                utils.setCdLatch(cdLatch);
                utils.RequestLogin(mUsername, mPassword, new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONObject jsonResult) {
                        jsonLoginResponse = jsonResult;
                        loginValidated = true;
                    }
                    @Override
                    public void onFailure(JSONObject jsonResult) {
                        jsonLoginResponse = jsonResult;
                        loginValidated = false;
                    }

                    @Override
                    public void onDeleted(String deletedMessage) {
                        //
                    }
                });
                try{
                    cdLatch.await(5000, TimeUnit.MILLISECONDS);
                }catch (InterruptedException ex){
                    ex.printStackTrace();
                }
                return loginValidated;
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean loginSuccess){
            mAuthTask = null;
            showProgress(false);

            if(loginSuccess){
                Intent terminalActivity = new Intent(LoginActivity.this, TerminalClientActivity.class);
                try{
                    terminalActivity.putExtra("jwt_token", jsonLoginResponse.getString("jwt_token"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                LoginActivity.this.startActivity(terminalActivity);
            }
            else{
                promptMessage(jsonLoginResponse);
            }
        }

        @Override
        protected void onCancelled() { showProgress(false); mAuthTask = null; }
    }

    private void promptMessage(JSONObject failedLoginResponse){
        final AlertDialog.Builder failedLoginAlert = new AlertDialog.Builder(LoginActivity.this);
        if(failedLoginResponse != null){
            failedLoginAlert.setMessage(failedLoginResponse.toString());
        }else{
            failedLoginAlert.setMessage("No response. Connection error");
        }
        failedLoginAlert.setTitle("Error");
        failedLoginAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        failedLoginAlert.setCancelable(true);
        failedLoginAlert.create().show();
    }
}
























