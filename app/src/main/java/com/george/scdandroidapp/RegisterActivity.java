package com.george.scdandroidapp;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import org.json.JSONObject;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class RegisterActivity extends AppCompatActivity {
    Utils utils;
    RegisterTask mRegisterTask;

    private EditText mUsernameView;
    private EditText mPasswordView;
    private CheckBox mIsTerminalChkBox;
    private Button   mRegisterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        utils = new Utils(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mUsernameView = (EditText) findViewById(R.id.txtUsernameReg);
        mPasswordView = (EditText) findViewById(R.id.txtPasswordReg);
        mIsTerminalChkBox = (CheckBox) findViewById(R.id.chkBoxIsTerminal);
        mRegisterButton = (Button) findViewById(R.id.btnRegisterReg);

        mRegisterButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                attemptRegister();
            }
        });
    }

    private void attemptRegister(){
        View focusView = null;
        boolean cancel = false;

        if(mRegisterTask != null){
            return;
        }
        System.out.println("ATTEMPTING REGISTER");
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store the values at the time of the login attempt
        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();
        boolean isTerminal = mIsTerminalChkBox.isChecked();

        if(!isUsernameValid(username)){
            mUsernameView.setError(getString(R.string.error_invalid_username)); // get username error string
            focusView = mUsernameView;
            cancel = true;
        }else if(!isPasswordValid(password)){
            mPasswordView.setError(getString(R.string.error_invalid_password)); // get password error string
            focusView = mPasswordView;
            cancel = true;
        }

        if(cancel){
            // An error occured. Don't attempt login and focus the first form field with an error
            if (focusView != null) {
                focusView.requestFocus();
            }
        }else{
            // Show Progress Spinner and kick off a background task to perform the user login attempt
            System.out.println(username + " " + password);
            mRegisterTask = new RegisterTask(username, password, isTerminal);
            mRegisterTask.execute((Void) null);
        }
    }

    private boolean isUsernameValid(String username){
        if(TextUtils.isEmpty(username)){
            return false;
        }
        return true;
    }

    private boolean isPasswordValid(String password){
        if(TextUtils.isEmpty(password)){
            return false;
        }else if(password.length()<5){
            return false;
        }
        return true;
    }

    public class RegisterTask extends AsyncTask<Void, Void, Boolean>{
        CountDownLatch cdLatch = new CountDownLatch(1);

        private final String mUsername;
        private final String mPassword;
        private final Boolean mIsTerminal;
        private boolean registerValidated;
        JSONObject jsonRegisterResponse;

        RegisterTask(String username, String password, Boolean isTerminal){
            this.mUsername = username;
            this.mPassword = password;
            this.mIsTerminal = isTerminal;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            if(utils != null){
                utils.setCdLatch(cdLatch);
                utils.RequestRegister(mUsername, mPassword, mIsTerminal, new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONObject jsonResult) {
                        registerValidated = true;
                        jsonRegisterResponse = jsonResult;
                    }
                    @Override
                    public void onFailure(JSONObject jsonResult) {
                        registerValidated = false;
                        jsonRegisterResponse = jsonResult;
                    }

                    @Override
                    public void onDeleted(String deletedMessage) {
                        //
                    }
                });
                try{
                    cdLatch.await(5000, TimeUnit.MILLISECONDS);
                }catch (InterruptedException ex){
                    ex.printStackTrace();
                }
                return registerValidated;
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean registerSuccess){
            mRegisterTask = null;
            promptMessage(registerSuccess, jsonRegisterResponse);
        }

        @Override
        protected void onCancelled(){ mRegisterTask = null; };
    }

    private void promptMessage(Boolean registerSuccess, JSONObject failedRegisterResponse){
        if(registerSuccess){
            final AlertDialog.Builder successRegisterAlert = new AlertDialog.Builder(RegisterActivity.this);
            successRegisterAlert.setMessage("User registered successfully!");
            successRegisterAlert.setTitle("SUCCESSFUL REGISTRATION");
            successRegisterAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    finish();       // Close current Register activity
                }
            });
        }else {
            final AlertDialog.Builder failedRegisterAlert = new AlertDialog.Builder(RegisterActivity.this);
            failedRegisterAlert.setMessage((CharSequence) failedRegisterResponse);
            failedRegisterAlert.setTitle("Error");
            failedRegisterAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            failedRegisterAlert.setCancelable(true);
            failedRegisterAlert.create().show();
        }
    }
}
